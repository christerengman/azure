
if (-not (Get-AzureRmContext).Account) {
    Login-AzureRmAccount
}

Get-AzureRmTenant | ForEach-Object {
    Write-Output ("Tenant:  {0} ({1})" -f ($_.Directory, $_.Id))
    [void] (Set-AzureRmContext -TenantId $_.Id)
    Get-AzureRmSubscription | ForEach-Object {
        Write-Output ("Subscription:  {0} ({1})" -f ($_.Name, $_.Id))
        [void] (Set-AzureRmContext -SubscriptionId $_.Id)
        Get-AzureRmResourceGroup | ForEach-Object {
            Write-Output ("Resource Group:  {0}" -f ($_.ResourceGroupName))
            Find-AzureRmResource -ResourceGroupName $_.ResourceGroupName | Format-Table -Property Name, ResourceType, Location
        }
    }
}


